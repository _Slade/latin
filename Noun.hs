{-# LANGUAGE OverloadedStrings #-}
module Noun where
import qualified Data.Text as T
import Data.Text (Text)
import Data.Char (chr, ord)
default(Integer, Double, Text)

data Declension
    = First
    | Second
    | Third
    | Fourth
    | Fifth
    | Sixth
    deriving (Eq, Ord, Show, Enum)

data Gender = Male | Female | Neuter deriving (Eq, Ord, Show)

data Noun = Noun {
    gender :: Gender,
    declension :: Declension,
    stem :: Text
} deriving (Eq, Show)

data IrregularNoun = IrregularNoun Noun [(Declension, Text)] deriving (Eq, Show)

data Case
    = Nominative
    | Genitive
    | Dative
    | Accusative
    | Ablative
    | Vocative
    deriving (Eq, Ord, Show, Enum)

data Number = Singular | Plural deriving (Eq, Ord, Show, Enum)

data Suffixes = Suffixes {
    getNom :: Text,
    getGen :: Text,
    getDat :: Text,
    getAcc :: Text,
    getAbl :: Text,
    getVoc :: Text
} deriving ()

instance (Show Suffixes) where
    show (Suffixes a b c d e f) = T.unpack $ T.unwords [a, b, c, d, e, f]

getSfxs :: Declension -> Gender -> Number -> Case -> Suffixes
getSfxs decl gender num ncase =
    case decl of
        First  -> case num of --       Nom   Gen     Dat   Acc   Abl   Voc
            Singular       -> Suffixes "a"   "ae"    "ae"  "am"  "ā"   "a"
            Plural         -> Suffixes "ae"  "ārum"  "īs"  "ās"  "īs"  "ae"
        Second -> case num of
            Singular ->
                case gender of
                    Neuter -> Suffixes "um"  "ī"     "ō"   "um"  "ō"   "um"
                    _      -> Suffixes "us"  "ī"     "ō"   "um"  "ō"   "e"
            Plural   ->
                case gender of
                    Neuter -> Suffixes "a"   "ōrum"  "īs"  "a"   "īs"  "a"
                    _      -> Suffixes "ī"   "ōrum"  "īs"  "ōs"  "īs"  "ī"
        Third  -> undefined
        Fourth -> undefined
        Fifth  -> undefined
        Sixth  -> undefined

{- Wrapper for getSfxs that takes the necessary properties from a Noun.  -}
getNounSfxs :: Noun -> Number -> Case -> Suffixes
getNounSfxs (Noun gender decl _) num ncase = getSfxs decl gender num ncase

main = mapM_ (putStrLn . show) 
    [ getSfxs d g n c | 
        d <- [First .. Sixth], 
        g <- [Male, Female, Neuter], 
        n <- [Singular, Plural], 
        c <- [Nominative .. Vocative]
    ]
